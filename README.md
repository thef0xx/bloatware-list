# Bloatware List

This repository aims to document and maintain a list of pre-installed bloatware applications found on popular phone company smartphones.
The bloatware list in this repository is compiled by gathering information from various sources. 

## Table Of Contents 
- [Xiaomi](./xiaomi/README.md)
- [Google](./google/README.md)
- [Oppo](./oppo/README.md)