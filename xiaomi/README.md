`com.android.browser` | Browser

`com.android.calendar` | Calendar

`com.android.soundrecorder` | Sound Recorder


**Facebook :**
`com.facebook.appmanager`,
`com.facebook.katana`,
`com.facebook.services`,
`com.facebook.system`

`com.mi.android.globalFileexplore`| File Explorer

`com.miui.audiomonitor`

`com.miui.bugreport`

`com.miui.calculator` | Calculator

`com.miui.cleanmaster`

`com.miui.compass` | Compass

`com.miui.fm` | FM Radio

`com.miui.fmservice`

`com.miui.mishare.connectivity` | Mi Share

`com.miui.misound` | Audio PLayer

`com.miui.notes` | Notes

`com.miui.player`

`com.miui.screenrecorder`| Screen Recorder

`com.miui.translationservice`

`com.miui.videoplayer` | Video Player

`com.miui.weather2` | Weather

**Netflix :** `com.netflix.mediaclient` ,`com.netflix.partner.activation`

`com.xiaomi.midrop` | Mi Drop

`com.xiaomi.payment`

`com.xiaomi.scanner` | Scanner

`com.duokan.phone.remotecontroller` | Mi Remote

`cn.wps.xiaomi.abroad.lite` | Doc Viewer

`com.miui.hybrid` | Mi Quick Apps (Spyware)

`com.micredit.in` | Mi Credit

`com.miui.miwallpaper` | Mi Wallpaper (Removing this breaks wallpaper functionality)

`com.android.providers.calendar` (Removing this breaks calendar funtionality)

`com.miui.securityadd` | Mi Security (Causes Bootloop)

`com.xiaomi.joyose` | (Manages SMS, Removing this will limit the Game Speed Turbo Booster) [Source](https://www.reddit.com/r/Xiaomi/comments/okdid3/i_finally_found_out_the_purpose_of_the_joyose_app/) 

`com.miui.msa.global` | Ad provider

`com.miui.daemon`

`com.xiaomi.mircs`

`com.xiaomi.mi_connect_service`

`com.xiaomi.discover` | System App Updater

`com.miui.yellowpage` | [YellowPages](https://en.wikipedia.org/wiki/Yellow_pages)

`com.miui.cloudservice` | Cloudservice

`com.miui.android.fashiongallery` | Wallpaper Carousel

`com.mi.health` | Mi Health

`com.mi.globalTrendNews`



