`com.google.android.gm` | Gmail

`com.google.android.gms` | Gmail Service

`com.google.android.gms.location.history` | Gmail Location Service

`com.google.android.apps.docs` | Google Docs

`com.google.android.apps.maps` | Google Maps

`com.google.android.apps.photos` | Google Photos

`com.google.android.apps.meetings` | Google Meet

`com.google.android.music` | Google Play Music

`com.android.chrome` | Google Chrome

`com.google.android.apps.photos` | Google Photos

`com.android.hotwordenrollment.okgoogle` | Google Assistant

`com.google.android.apps.nbu.paisa.user` | Google Pay

`com.google.android.googlequicksearchbox` | Google Quick Search

`com.google.android.inputmethod.latin` | Gboard

`com.google.android.apps.wellbeing` | Digital Wellbeing

`com.google.android.tts` | Text-to-speech

`com.google.android.feedback` | Feedback

`com.google.android.keep` | Google Keep

`com.google.android.apps.nbu.files` | Google Files

`com.google.android.youtube` | Youtube

`com.google.android.printservice.recommendation` | Mobile Printing

`com.google.android.syncadapters.calendar` | Calendar Sync

`com.google.ar.lens` | AR Lens

`com.google.ar.core` | AR Core

`com.google.android.projection.gearhead` | Android Auto
