`com.coloros.aftersalesservice`

`com.coloros.alarmclock` | Alarm

`com.coloros.compass2` | Compass

`com.coloros.safesdkproxy` | Antivirus

`com.coloros.soundrecorder` | Voice Recorder

`com.coloros.screenrecorder` | Screen Recorder

`com.coloros.video` | Video Player

`com.coloros.widget.smallweather` | Weather

`com.coloros.weather.service` | Weather

`com.heytap.themestore` | Theme Store

`com.nearme.atlas`

`com.netflix.mediaclient`

`com.netflix.partner.activation`

`com.tencent.soter.soterserver`

`com.opera.preinstall` | Opera Browser

`com.oppo.atlas` | Malware

`com.oppo.market` | Oppo App Market

`com.oppo.music` | Music

`com.oppo.quicksearchbox` | Search

`com.oppo.gmail.overlay`

`com.heytap.cloud` | HeyTap Cloud

`com.oppo.partnerbrowsercustomizations` | Browser Bookmarks
